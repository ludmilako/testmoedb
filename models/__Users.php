<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Users".
 *
 * @property integer $ID
 * @property string $userName
 * @property string $authKey
 * @property string $password
 * @property string $firsitName
 * @property string $lastName
 * @property string $email
 * @property integer $phone
 * @property string $role
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'userName', 'authKey', 'password', 'firsitName', 'lastName', 'email', 'phone', 'role'], 'required'],
            [['ID', 'phone'], 'integer'],
            [['userName', 'authKey', 'password', 'firsitName', 'lastName', 'email', 'role'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'userName' => 'User Name',
            'authKey' => 'Auth Key',
            'password' => 'Password',
            'firsitName' => 'Firsit Name',
            'lastName' => 'Last Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'role' => 'Role',
        ];
    }
}
